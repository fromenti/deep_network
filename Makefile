CPP = g++
EXE = deep-network
DEBUG = -g -DDEBUG #-Q # for gdb
CPP_FLAG = -Isrc  -Wfatal-errors -march=native $(DEBUG)
OBJ_FILES = obj/mnist.o obj/network.o obj/full_connected.o obj/convolution.o #obj/pooling.o

all: $(EXE)

$(EXE): $(OBJ_FILES) src/main.cpp
	$(CPP) $(CPP_FLAG) $^ -o $@

obj/network.o: src/network.cpp src/network.hpp src/layers/activation.hpp  obj/full_connected.o obj/convolution.o #obj/pooling.o
	$(CPP) $(CPP_FLAG) -c $< -o $@

obj/mnist.o: src/mnist/mnist.cpp src/mnist/mnist.hpp src/dataset.hpp src/vector.hpp
	$(CPP) $(CPP_FLAG) -c $< -o $@

obj/full_connected.o: src/layers/full_connected.cpp src/layers/full_connected.hpp src/layers/layer.hpp src/vector.hpp
	$(CPP) $(CPP_FLAG) -c $< -o $@

obj/convolution.o: src/layers/convolution.cpp src/layers/convolution.hpp src/layers/layer.hpp src/vector.hpp
	$(CPP) $(CPP_FLAG) -c $< -o $@

obj/pooling.o: src/layers/pooling.cpp src/layers/pooling.hpp src/layers/layer.hpp src/vector.hpp
	$(CPP) $(CPP_FLAG) -c $< -o $@

clean:
	-$(RM) -r obj/* doc/* $(EXE) $(EXE)-main.cpp*

docs:
	@doxygen
