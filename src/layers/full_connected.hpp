#ifndef FULL_CONNECTED_LAYER_HPP
#define FULL_CONNECTED_LAYER_HPP


#include <random>
#include "layer.hpp"

namespace Layer{
  class FullConnectedLayer:public Layer{
  public:
    Vector b;
    Vector w;
    Vector nabla_b;
    Vector nabla_w;
    FullConnectedLayer(size_t n,size_t m);
    ~FullConnectedLayer();
    void init_standard();
    void init(Real mu,Real sigma);
    Vector feed_forward(Vector x) override;
    void init_nabla() override;
    Vector back_propagation(Vector e) override;
    void update(Real eta) override;
  };
}
#endif
