#ifndef CONVOLUTION_LAYER_HPP
#define CONVOLUTION_LAYER_HPP

#include <random>
#include "layer.hpp"
#include <cstdint>
#include "avx.hpp"
namespace Layer{
  /*****************************************
   * Implementation of a convolutionnal Layer

   */
  class ConvolutionLayer:public Layer{
  private:
    size_t nf,ni,nj;
    size_t mf,mi,mj;
    size_t p,q;
    Vector K;
    Vector b;
    Vector nabla_K;
    Vector nabla_b;
    v8i* vindex1;

  public:
    ConvolutionLayer(size_t nf,size_t ni,size_t nj,size_t p,size_t q,size_t mf);
    ~ConvolutionLayer();
    void init(Real mu,Real sigma);
    Vector feed_forward(Vector x) override;
    Vector avx_feed_forward(Vector x);
    void init_nabla() override;
    Vector back_propagation(Vector e) override;
    void update(Real eta) override;
  };
}
#endif
