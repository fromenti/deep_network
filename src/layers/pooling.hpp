#ifndef POOLING_LAYER_HPP
#define POOLING_LAYER_HPP

#include "layer.hpp"

namespace Layer{

  class Pooling:public Layer{
  public:
    size_t nf;
    size_t ni;
    size_t nj;
    size_t p;
    size_t q;
    size_t mi;
    size_t mj;
  public:
    Pooling(size_t nf,size_t ni,size_t nj,size_t p,size_t q);
    ~Pooling(){};
    Vector feed_forward(Vector x) override;
    void init_nabla() override {};
    Vector back_propagation(Vector e) override;
    void update(Real) override {};
  };

  inline
  Pooling::Pooling(size_t nf_,size_t ni_,size_t nj_,size_t p_,size_t q_):
  Layer(nf_*ni_*nj_,nf_*((ni_+p_-1)/p_)*((nj_+q_-1)/q_)){
    nf=nf_;
    ni=ni_;
    nj=nj_;
    p=p_;
    q=q_;
    mi=(ni+p-1)/p;
    mj=(nj+q-1)/p;
  }

}

#endif
