#ifndef DEBUG_HPP
#define DEBUG_HPP

#include <iostream>
using namespace std;

#ifdef DEBUG
#define assert(cond) if(!(cond)){cout<<"Assertion failed @ "<<__FILE__<<":"<<__LINE__<<endl;exit(0);}
#else
#define NDEBUG
#define assert(cond)
#endif

#endif
