#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "debug.hpp"

class Shape{
private:
  size_t dim;
  size_t* n;
public:
  Shape(size_t n);
  Shape(size_t n1,size_t n2);
  Shape(size_t n1,size_t n2,size_t n3);
  size_t size() const;
};

inline
Shape::Shape(size_t n1){
  dim=1;
  n=new size_t[1];
  n[0]=n1;
}

inline
Shape::Shape(size_t n1,size_t n2){
  dim=2;
  n=new size_t[2];
  n[0]=n1;n[1]=n2;
}

inline
Shape::Shape(size_t n1,size_t n2,size_t n3){
  dim=3;
  n=new size_t[3];
  n[0]=n1;n[1]=n2,n[2]=n3;
}

inline size_t
Shape::size() const{
  size_t s=1;
  for(size_t d=0;d<dim;++d) s*=n[d];
  return s;
}


#endif
