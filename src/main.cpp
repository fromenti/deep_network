#include <iomanip>
#include "layers/layers.hpp"
#include "network.hpp"
#include "mnist/mnist.hpp"
#include <chrono>
#include "avx.hpp"

using namespace Layer;


int main(int argc,char** argv){
  size_t nf=4;
  ConvolutionLayer L(1,28,28,5,5,nf);
  L.init(0,1);
  Mnist dataset;
  pair<Vector,Vector> data=dataset.get_train(0);
  Vector y=L.feed_forward(data.first);
  exit(0);


  Network N;
  //size_t nf=4;
  ConvolutionLayer L1(1,28,28,5,5,nf);
  L1.init(0,1);
  ActivationLayer<Sigmoid> L2(nf*24*24);
  //Layer::Pooling L3(nf,24,24,2,2);
  FullConnectedLayer L4(nf*24*24,10);
  L4.init_standard();
  ActivationLayer<Sigmoid> L5(10);
  L1.name="[Convolutionnal]";
  L2.name="[Sigmoid of convolutionnal]";
  //L3.name="[Pooling]";
  L4.name="[Full connected]";
  L5.name="[Sigmoid of full]";

  N.push_layer(L1);
  N.push_layer(L2);
  //    N.push_layer(&L3);
  N.push_layer(L4);
  N.push_layer(L5);
  N.is_done();
  //Mnist dataset;
  N.train(&dataset,1,10,0.1);
}
