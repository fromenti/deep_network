#ifndef DATASET_HPP
#define DATASET_HPP

#include <cstddef>
#include "vector.hpp"

using namespace std;

class Dataset{
protected:
  size_t train_size;
  size_t test_size;
  size_t x_size;
  size_t y_size;
public:
  Dataset();
  size_t get_train_size() const;
  size_t get_test_size() const;
  size_t get_y_size() const;
  virtual pair<Vector,Vector> get_train(const size_t i) const=0;
  virtual pair<Vector,Vector> get_test(const size_t i) const=0;
};

inline
Dataset::Dataset(){
}

inline size_t
Dataset::get_train_size() const{
  return train_size;
}

inline size_t
Dataset::get_test_size() const{
  return test_size;
}

inline size_t
Dataset::get_y_size() const{
  return y_size;
}

#endif
