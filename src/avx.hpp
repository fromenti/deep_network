#ifndef AVX_HPP
#define AVX_HPP

#include <cstdint>

union v8f{
  __m256 avx;
  float f[8];
};

union v8i{
  __m256 avx;
  uint32_t i[8];
};


float hadd(const v8f& u);
void display(const v8f& u);


inline float
hadd(const v8f& u){
  v8f v={_mm256_hadd_ps(u.avx,u.avx)};
  return v.f[0]+v.f[2]+v.f[4]+v.f[6];
}



inline void
display(const v8f& u){
  cout<<'['<<u.f[0];
  cout<<','<<u.f[1];
  cout<<','<<u.f[2];
  cout<<','<<u.f[3];
  cout<<','<<u.f[4];
  cout<<','<<u.f[5];
  cout<<','<<u.f[6];
  cout<<','<<u.f[7];
  cout<<','<<']';
}

#endif
